<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $shaun = new Animal("shaun");
    echo "name  :  ".$shaun->nama. "<br>";
    echo "legs :  ".$shaun->legs. "<br>"; 
    echo "cold blooded :  ".$shaun->cold_blooded. "<br> <br>"; 
    
    $kodok = new Frog("buduk");
    echo "name  :  ".$kodok->nama. "<br>";
    echo "legs :  ".$kodok->legs. "<br>"; 
    echo "cold blooded  :  ".$kodok->cold_blooded. "<br>";
    echo $kodok->jump();

    $sungkong = new Ape("kerasakti");
    echo "name  :  ".$sungkong->nama. "<br> <br>";
    echo "legs :  ".$sungkong->legs. "<br>"; 
    echo "cold blooded  :  ".$sungkong->cold_blooded. " <br>";
    echo $sungkong->yell();
?>